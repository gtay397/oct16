create database newdatabase;

create table animals (
    id integer not null primary key,
    name varchar (30),
    shortDescription varchar (150),
    longDescription varchar (500),
    cost int,
    image varchar (500),
)

create table users (
    username varchar (30) not null primary key,
    password varchar (15) not null primary key,
)

create table orders (
    id integer not null,
    itemname varchar (30),
    username varchar (30) not null,
    timestamp timestamp,
    totalCost integer,
    foreign key (username) references users (username),
    foreign key (itemname) references animals (name),

)